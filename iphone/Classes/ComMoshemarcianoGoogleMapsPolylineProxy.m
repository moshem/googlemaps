/**
 * Appcelerator Titanium Mobile
 * Copyright (c) 2009-2013 by Appcelerator, Inc. All Rights Reserved.
 * Licensed under the terms of the Apache Public License
 * Please see the LICENSE included with this distribution for details.
 */

#import "ComMoshemarcianoGoogleMapsPolylineProxy.h"
#import "TiUtils.h"

@implementation ComMoshemarcianoGoogleMapsPolylineProxy
@synthesize polyline;

-(GMSPolyline *)polyline
{
    if (polyline==nil)
    {
        polyline = [[GMSPolyline alloc] init];
    }
    
    return polyline;
}

-(void)dealloc
{
   //   NSLog(@"[INFO] [GoogleMapsModule] Google Maps => ComMoshemarcianoGoogleMapsPolylineProxy dealloc");
    polyline = nil;
    //   [super dealloc];
}

-(void)setGeodesic:(id)value
{
    ENSURE_UI_THREAD(setGeodesic, value);
    
    if ([self polyline])
    {
        polyline.geodesic = [TiUtils boolValue:value];
    }
}

-(void)setWidth:(id)value
{
    ENSURE_UI_THREAD(setWidth, value);
    
    if ([self polyline])
    {
        polyline.strokeWidth = [TiUtils floatValue:value];
    }
}

-(void)setColor:(id)value
{
    ENSURE_UI_THREAD(setColor, value);
    
    if ([self polyline])
    {
        polyline.strokeColor = [[TiUtils colorValue:value] _color];
    }
}

-(void)setAccessibilityLabel:(id)value
{
    ENSURE_UI_THREAD(setAccessibilityLabel, value);
    
    if ([self polyline])
    {
        polyline.accessibilityLabel = [TiUtils stringValue:value];
    }
}

-(void)setPath:(id)value
{
    ENSURE_UI_THREAD(setPath, value);
    
    NSArray *a = (NSArray *)value;
    
    if ([self polyline])
    {
        
        NSInteger itemCount = [value count];
        if (itemCount % 2)
        {
            NSLog (@"[GoogleMapsModule] uneven number of path data passed, aborting");
            return;
        }
        
        GMSMutablePath *path = [GMSMutablePath path];
        
        for (NSInteger i=0;i<itemCount;i+=2)
        {
           // NSLog(@"=============> object pair passed: %f, %f",  [value[i] doubleValue],  [value[i+1] doubleValue]);
            
            [path addCoordinate:CLLocationCoordinate2DMake([value[i] doubleValue], [value[i+1] doubleValue])];
        }

        polyline.path = path;        
    }
}


@end
