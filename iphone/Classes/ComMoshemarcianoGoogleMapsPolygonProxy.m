/**
 * Appcelerator Titanium Mobile
 * Copyright (c) 2009-2013 by Appcelerator, Inc. All Rights Reserved.
 * Licensed under the terms of the Apache Public License
 * Please see the LICENSE included with this distribution for details.
 */

#import "ComMoshemarcianoGoogleMapsPolygonProxy.h"
#import "TiUtils.h"

@implementation ComMoshemarcianoGoogleMapsPolygonProxy

@synthesize polygon;

-(GMSPolygon *)polygon
{
    if (polygon==nil)
    {
        polygon = [[GMSPolygon alloc] init];
    }
    
    return polygon;
}

-(void)setGeodesic:(id)value
{
    ENSURE_UI_THREAD(setGeodesic, value);
    
    if ([self polygon])
    {
        polygon.geodesic = [TiUtils boolValue:value];
    }
}

-(void)setTitle:(id)value
{
    ENSURE_UI_THREAD(setTitle, value);

    if ([self polygon])
    {
        polygon.title = [TiUtils stringValue:value];
    }
}

-(void)setTappable:(id)value
{
    ENSURE_UI_THREAD(setTappable, value);
    
    if ([self polygon])
    {
        polygon.tappable = [TiUtils boolValue:value];
    }
}

-(void)setWidth:(id)value
{
    ENSURE_UI_THREAD(setWidth, value);
    
    if ([self polygon])
    {
        polygon.strokeWidth = [TiUtils floatValue:value];
    }
}

-(void)setColor:(id)value
{
    ENSURE_UI_THREAD(setColor, value);
    
    if ([self polygon])
    {
        polygon.strokeColor = [[TiUtils colorValue:value] _color];
    }
}


-(void)setFillColor:(id)value
{
    ENSURE_UI_THREAD(setFillColor, value);

    if ([self polygon])
    {
        polygon.fillColor = [[TiUtils colorValue:value] _color];
    }
}

-(void)setAccessibilityLabel:(id)value
{
    ENSURE_UI_THREAD(setAccessibilityLabel, value);
    
    if ([self polygon])
    {
        polygon.accessibilityLabel = [TiUtils stringValue:value];
    }
}

-(void)setPath:(id)value
{
    ENSURE_UI_THREAD(setPath, value);
    
    NSArray *a = (NSArray *)value;
    
    if ([self polygon])
    {
        
        NSInteger itemCount = [value count];
        if (itemCount % 2)
        {
            NSLog (@"[GoogleMapsModule] uneven number of path data passed, aborting");
            return;
        }
        
        GMSMutablePath *path = [GMSMutablePath path];
        
        for (NSInteger i=0;i<itemCount;i+=2)
        {
            //NSLog(@"=============> object pair passed: %f, %f",  [value[i] doubleValue],  [value[i+1] doubleValue]);
            
            [path addCoordinate:CLLocationCoordinate2DMake([value[i] doubleValue], [value[i+1] doubleValue])];
        }

        polygon.path = path;        
    }
}


@end
