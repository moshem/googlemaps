/**
 * Appcelerator Titanium Mobile
 * Copyright (c) 2009-2013 by Appcelerator, Inc. All Rights Reserved.
 * Licensed under the terms of the Apache Public License
 * Please see the LICENSE included with this distribution for details.
 */
#import "TiProxy.h"
#import <GoogleMaps/GoogleMaps.h>


@interface ComMoshemarcianoGoogleMapsMarkerProxy : TiProxy {
    GMSMarker *marker;
    id myUserData;
}

@property(nonatomic) GMSMarker *marker;
@property (nonatomic, strong) id myUserData;

@end
