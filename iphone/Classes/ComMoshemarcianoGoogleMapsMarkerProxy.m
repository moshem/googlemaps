/**
 * Appcelerator Titanium Mobile
 * Copyright (c) 2009-2013 by Appcelerator, Inc. All Rights Reserved.
 * Licensed under the terms of the Apache Public License
 * Please see the LICENSE included with this distribution for details.
 */

#import "ComMoshemarcianoGoogleMapsMarkerProxy.h"
#import "TiUtils.h"
#import "TiApp.h"
#import <QuartzCore/QuartzCore.h>

@implementation ComMoshemarcianoGoogleMapsMarkerProxy
@synthesize marker, myUserData;


-(void)dealloc
{
    NSLog(@"[INFO] [GoogleMapsModule] Google Maps => ComMoshemarcianoGoogleMapsMarkerProxy dealloc");
    marker.userData = nil;
    marker.title = nil;
    marker.snippet = nil;
    marker.icon = nil;
    self.marker = nil;
    self.myUserData = nil;
 //   [super dealloc];
}

-(GMSMarker *)marker
{ 
    if (marker==nil)
    {
        marker = [[GMSMarker alloc] init];
        
        marker.userData = self;
    }
    
    return marker;
}



-(void)setZIndex:(id)value
{
    ENSURE_UI_THREAD(setZIndex, value);
    
    
    if ([self marker])
    {
        self.marker.zIndex = [TiUtils intValue:value];
    }
}


-(void)setFlat:(id)value
{
    ENSURE_UI_THREAD(setFlat, value);
    
    
    if ([self marker])
    {
        self.marker.flat = [TiUtils intValue:value];
    }
}

-(void)setGroundAnchor:(id)value
{
    ENSURE_UI_THREAD(setGroundAnchor, value);
    
    
    if ([self marker])
    {
         NSLog(@"[GoogleMapsModule] Anchor");
        self.marker.groundAnchor = CGPointMake([[value objectForKey:@"x"] doubleValue],
                                               [[value objectForKey:@"y"] doubleValue]);
    }
}



-(void)setDraggable:(id)value
{
    ENSURE_UI_THREAD(setDraggable, value);
    
    
    if ([self marker])
    {
        self.marker.draggable = [TiUtils intValue:value];
    }
}
    
-(void)setRotation:(id)value
{
    ENSURE_UI_THREAD(setRotation, value);
    
    if ([self marker])
    {
        self.marker.rotation = [TiUtils intValue:value];
    }
}
    
-(void)setLocation:(id)value
{
    ENSURE_UI_THREAD(setLocation, value);
    
    if ([self marker])
    {
        self.marker.position = CLLocationCoordinate2DMake([[value objectForKey:@"latitude"] doubleValue],[[value objectForKey:@"longitude"] doubleValue]);
    }
}

-(void)setAnimated:(id)value
{
    ENSURE_UI_THREAD(setAnimated, value);
    
    if ([self marker])
    {
        if ([TiUtils boolValue:value])
            marker.appearAnimation = kGMSMarkerAnimationPop;
        else
            marker.appearAnimation = kGMSMarkerAnimationNone;
    }
}

-(void)setTintColor:(id)value
{
    ENSURE_UI_THREAD(setTintColor, value);
    
    if ([self marker])
    {
        marker.icon = [GMSMarker markerImageWithColor:[[TiUtils colorValue:value] _color]];
    }
}

-(void)setTappable:(id)value
{
    ENSURE_UI_THREAD(setTappable, value);
    
    if ([self marker])
    {
        marker.tappable = [TiUtils boolValue:value];
    }
}


-(void)setTitle:(id)value
{
    ENSURE_UI_THREAD(setTitle, value);
    
    if ([self marker])
    {
        marker.title = [TiUtils stringValue:value];
    }
}

-(void)setSnippet:(id)value
{
    ENSURE_UI_THREAD(setSnippet, value);
    
    if ([self marker])
    {
        marker.snippet = [TiUtils stringValue:value];
    }
}

-(void)setUserData:(id)value
{
   ENSURE_UI_THREAD(setUserData, value);

   if ([self marker])
    {
        ENSURE_SINGLE_ARG(value, NSObject);

        self.myUserData = value;
        // NSLog (@"setting user data2: %@", self.myUserData);
    }
}


- (UIImage *)imageForBrightness:(CGFloat)brightness
                            hue:(CGFloat)hue {
    static int count = 0;
    int w = 192 + rand()%64, h = 192 + rand()%64;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, w, h)];
    view.backgroundColor = [UIColor colorWithHue:hue
                                      saturation:1.0f
                                      brightness:brightness
                                           alpha:1.0f];
    view.layer.borderWidth = 2;
    view.layer.cornerRadius = 4;
    view.layer.borderColor = [[UIColor blackColor] CGColor];
    
    UILabel *label = [[UILabel alloc] initWithFrame:view.frame];
    label.text = [NSString stringWithFormat:@"%d: %.2f", ++count, hue];
    label.textColor = [UIColor blackColor];
    label.backgroundColor = [UIColor colorWithWhite:0.f alpha:0.0f];
    label.textAlignment = NSTextAlignmentCenter;
    [view addSubview:label];
    
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, 0.f);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *resultingImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resultingImage;
}

-(void)setIcon:(id)value
{
    ENSURE_UI_THREAD(setIcon, value);
    
    if ([self marker])
    {
        NSString * filename = [TiUtils stringValue:value];
        NSURL* url = [NSURL URLWithString:filename];
        filename = [[TiHost resourcePath] stringByAppendingPathComponent:filename];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filename];
             
        if (fileExists)
        {
            marker.icon = [UIImage imageWithContentsOfFile:filename];
        }
        else
            NSLog(@"[GoogleMapsModule] icon image not found: %@",filename);
    }
}

-(void)setIconView:(id)value
{
    ENSURE_UI_THREAD(setIconView, value);
    
    if ([self marker])
    {
        TiViewProxy* viewProxy = value;
        
        if (viewProxy!=nil && [viewProxy isKindOfClass:[TiViewProxy class]])
        {
            marker.icon = [ [viewProxy toImage:nil] image];
        }
        else
            NSLog(@"[GoogleMapsModule] icon view is not compatible");
    }
}


-(id)data
{
    NSMutableDictionary *ret = [NSMutableDictionary dictionary];
    
    [ret setObject:[NSNumber numberWithFloat:marker.position.latitude] forKey:@"latitude"];
    [ret setObject:[NSNumber numberWithFloat:marker.position.longitude] forKey:@"longitude"];
    [ret setObject:marker.title forKey:@"title"];
    [ret setObject:marker.title forKey:@"snippet"];
    [ret setObject:self.userData forKey:@"userData"];
    [ret setObject:marker.userData forKey:@"marker"];
    
    return ret;
}

-(id)userData
{
    //   NSLog (@"getting user data: %@", self.myUserData);
    return self.myUserData;
}

-(id)title
{
    return marker.title;
}

-(id)snippet
{
    return marker.snippet;
}

-(id)location
{
    NSMutableDictionary *ret = [NSMutableDictionary dictionary];
    
    [ret setObject:[NSNumber numberWithFloat:marker.position.latitude] forKey:@"latitude"];
    [ret setObject:[NSNumber numberWithFloat:marker.position.longitude] forKey:@"longitude"];
    
    return ret;
}

@end
