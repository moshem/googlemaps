/**
 * Appcelerator Titanium Mobile
 * Copyright (c) 2009-2013 by Appcelerator, Inc. All Rights Reserved.
 * Licensed under the terms of the Apache Public License
 * Please see the LICENSE included with this distribution for details.
 */
#import "TiViewProxy.h"

@interface ComMoshemarcianoGoogleMapsGoogleMapProxy : TiViewProxy {
    NSString *sMapType;
    BOOL bHotFixVR0407;
}

@property(nonatomic,readwrite,assign) NSString * mapType;
@property(nonatomic,readwrite,assign) id selectedMarker;
@property(nonatomic,readwrite,assign) BOOL bHotFixVR0407;
    @property(nonatomic,readwrite,assign) NSString* propertyName;

@end
