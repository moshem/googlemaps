#import <GoogleMaps/GoogleMaps.h>
#import <UIKit/UIKit.h>

#import "TiUIView.h"

/**
 * MapSampleViewController is a simple UIViewController that describes a sample
 * (empty) Google Maps SDK for iOS demo. More complex demos may be built as
 * subclasses of this class.
 *
 * Note that the default loadView implementation just sets the whole view of
 * this class to mapView; i.e., self.view = self.mapView;.
 */
@interface MapSampleViewController : UIViewController <GMSMapViewDelegate>
{
    TiUIView *viewAttached;
    BOOL customInfoWindow;
    BOOL overrideCameraMoveOnTapMarker;
    BOOL isShutdown;
}

/** GMSMapView managed by this controller. */
@property (nonatomic, readonly, strong) GMSMapView *mapView;
@property (nonatomic, strong) TiUIView *viewAttached;
@property(nonatomic) BOOL customInfoWindow;
@property(nonatomic) BOOL isShutdown;
@property(nonatomic) BOOL overrideCameraMoveOnTapMarker;
@property (nonatomic, strong) KrollCallback *  customViewCallback;

@end
