/**
 * Appcelerator Titanium Mobile
 * Copyright (c) 2009-2013 by Appcelerator, Inc. All Rights Reserved.
 * Licensed under the terms of the Apache Public License
 * Please see the LICENSE included with this distribution for details.
 */

#import "ComMoshemarcianoGoogleMapsGoogleMap.h"
#import "ComMoshemarcianoGoogleMapsGoogleMapProxy.h"
#import "ComMoshemarcianoGoogleMapsMarkerProxy.h"
#import "ComMoshemarcianoGoogleMapsPolylineProxy.h"
#import "ComMoshemarcianoGoogleMapsPolygonProxy.h"
#import "ComMoshemarcianoGoogleMapsCircleProxy.h"

#import <GoogleMaps/GoogleMaps.h>

@implementation ComMoshemarcianoGoogleMapsGoogleMap

@synthesize /*selectedMarker,*/example,getMap,customInfoWindow;

-(GMSMapView *)getMap
{
    ENSURE_UI_THREAD_0_ARGS
    
    return map;
}


-(void)removeMap
{
    ENSURE_UI_THREAD_0_ARGS
    
    //NSLog(@"[INFO] [GoogleMapsModule] Google Maps => Release map and added objects");

    for (int i = 0; i < [markers count]; i++) {
        ComMoshemarcianoGoogleMapsMarkerProxy * p = [markers objectAtIndex: i];
        [self removeMarker:p];
    }
    
    for (int i = 0; i < [polylines count]; i++) {
        ComMoshemarcianoGoogleMapsPolylineProxy * p = [polylines objectAtIndex: i];
        [self removePolyline:p];
    }
    
    for (int i = 0; i < [polygones count]; i++) {
        ComMoshemarcianoGoogleMapsPolygonProxy * p = [polygones objectAtIndex: i];
        [self removePolygon:p];
    }
    
    for (int i = 0; i < [circles count]; i++) {
        ComMoshemarcianoGoogleMapsCircleProxy * p = [circles objectAtIndex: i];
        [self removeCircle:p];
    }
  
    
     [markers removeAllObjects];
     [circles removeAllObjects];
     [polylines removeAllObjects];
     [polygones removeAllObjects];
    
     [controller.view removeFromSuperview];
    
     markers = nil;
     circles = nil;
     polylines = nil;
     polygones = nil;
     map = nil;
     controller.view = nil;
     controller = nil;
    
     //RELEASE_TO_NIL(markers);
     //RELEASE_TO_NIL(circles);
     //RELEASE_TO_NIL(polylines);
     //RELEASE_TO_NIL(polygones);
     

}


-(void)viewDidDetach
{
    NSLog(@"[INFO] [GoogleMapsModule] Google Maps => view Detached");
    
    [self removeMap];
}

-(void)dealloc
{
   
//    NSLog(@"[INFO] [GoogleMapsModule] Google Maps => dealloc");

//    [self removeMap:nil];
    
    //RELEASE_TO_NIL(square);
    
    //[super dealloc];
}
/*
-(id)initWithFrame:(CGRect)aRect
{
    ENSURE_UI_THREAD_0_ARGS
    
    controller = [[MapSampleViewController alloc] init];
    NSLog(@"[INFO] 1");
    square = controller.view;
    NSLog(@"[INFO] 2");
    map = controller.mapView;
    NSLog(@"[INFO] 3");
    controller.viewAttached = self;
    NSLog(@"[INFO] 4");
    [self addSubview:square];
    NSLog(@"[INFO] 5");
    [TiUtils setView:square positionRect:self.bounds];
    NSLog(@"[INFO] 6");
    return (id)square;
}
*/

/*
-(MapSampleViewController*)controller
{
    if (controller==nil)
    {
        controller = [[MapSampleViewController alloc] init];
    }
}
*/
-(UIView*)square
{

    ENSURE_UI_THREAD_0_ARGS
    
   // if (controller.isShutdown == YES)
   //     return;
    
    //if (square == nil)
    {
        //square = [[UIView alloc] initWithFrame:[self frame]];
        controller = [[MapSampleViewController alloc] init];
        square = controller.view;
        
        map = controller.mapView;
        controller.viewAttached = self;
        
        [self addSubview:square];
        [TiUtils setView:square positionRect:self.bounds];
        
        // init arrays
        
        markers = [[NSMutableArray alloc] init];
        circles = [[NSMutableArray alloc] init];
        polylines = [[NSMutableArray alloc] init];
        polygones = [[NSMutableArray alloc] init];
        
        self->customViewCallback = nil;

    }

    //NSLog(@"    =====================================> init square : square = %@", square);

    return square;
}

-(void)frameSizeChanged:(CGRect)frame bounds:(CGRect)bounds
{
    //NSLog(@"frameSizeChanged");
    if (square!=nil)
    {
        [TiUtils setView:square positionRect:bounds];
    }
}
/*
-(void)setColor_:(id)color
{
    //UIColor *c = [[TiUtils colorValue:color] _color];
    //UIView *s = [self square];
    //s.backgroundColor = c;
}
*/
-(void)setCustomInfoWindow_:(id)args
{
    ENSURE_UI_THREAD(setCustomInfoWindow_, args);
    //if ([self square])
    {
        BOOL showPopover = [TiUtils boolValue:args def:NO];
        controller.customInfoWindow = showPopover;
    }
}



-(void)setCameraMoveOnMarkerTap_:(id)args
{
    ENSURE_SINGLE_ARG(args,NSObject);
    ENSURE_UI_THREAD(setCameraMoveOnMarkerTap_, args);
    
    //if ([self square])
    {
        BOOL override = [TiUtils boolValue:args def:NO];
        controller.overrideCameraMoveOnTapMarker = override;
    }
    
   // NSLog(@"==> in setCameraMoveOnMarkerTap_, map: %@", map);

}


-(void)setIndoorPicker_:(id)args
{
    ENSURE_UI_THREAD(setScrollGestures_, args);
    //if ([self square])
    {
        map.settings.indoorPicker = [TiUtils boolValue:args];
         NSLog(@"==> in setIndoorPicker_, map: %@", map);

    }
}


-(void)setScrollGestures_:(id)args
{
    ENSURE_UI_THREAD(setScrollGestures_, args);
    //if ([self square])
    {
        map.settings.scrollGestures = [TiUtils boolValue:args];
    }
}

-(void)setZoomGestures_:(id)args
{
    ENSURE_UI_THREAD(setZoomGestures_, args);
    //if ([self square])
    {
        map.settings.zoomGestures = [TiUtils boolValue:args];
    }
}

-(void)setTiltGestures_:(id)args
{
    ENSURE_UI_THREAD(setTiltGestures_, args);
    //if ([self square])
    {
        map.settings.tiltGestures = [TiUtils boolValue:args];
    }
}

-(void)setRotateGestures_:(id)args
{
    ENSURE_UI_THREAD(setRotateGestures_, args);
    //if ([self square])
    {
        map.settings.rotateGestures = [TiUtils boolValue:args];
    }
}



-(void)setMapType_:(id)args
{
    ENSURE_UI_THREAD(setMapType_, args);
    //if ([self square])
    {
        NSString *type = [TiUtils stringValue:args];
        
        if ([type isEqualToString: @"normal"])
            map.mapType = kGMSTypeNormal;
        else
            if ([type isEqualToString: @"hybrid"])
                map.mapType = kGMSTypeHybrid;
            else
                if ([type isEqualToString: @"satellite"])
                    map.mapType = kGMSTypeSatellite;
                else
                    if ([type isEqualToString: @"terrain"])
                        map.mapType = kGMSTypeTerrain;
            
//        ComMoshemarcianoGoogleMapsGoogleMapProxy *p = (ComMoshemarcianoGoogleMapsGoogleMapProxy*)self.proxy;
 //       p._mapType = [NSString stringWithString:type];

    }
}



-(void)setTraffic_:(id)args
{
    ENSURE_UI_THREAD(setTraffic_, args);
    //if ([self square])
    {
        map.trafficEnabled = [TiUtils boolValue:args];
    }
}

-(void)setMyLocation_:(id)args
{
    ENSURE_UI_THREAD(setMyLocation_, args);

    BOOL b = [TiUtils boolValue:args];
    
    if (b == YES) {
        dispatch_async(dispatch_get_main_queue(), ^{
            map.myLocationEnabled = YES;
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            map.myLocationEnabled = NO;
        });
    }
}

-(void)setMyLocationButton_:(id)args
{
    ENSURE_UI_THREAD(setMyLocationButton_, args);
    
    BOOL b = [TiUtils boolValue:args];
    
    if (b == YES) {
        dispatch_async(dispatch_get_main_queue(), ^{
            map.settings.myLocationButton = YES;
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            map.settings.myLocationButton = NO;
        });
    }
}

-(void)setCompassButton_:(id)args
{
    ENSURE_UI_THREAD(setCompassButton_, args);
    
    BOOL b = [TiUtils boolValue:args];
    
    if (b == YES) {
        dispatch_async(dispatch_get_main_queue(), ^{
            map.settings.compassButton = YES;
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            map.settings.compassButton = YES;
        });
    }
}


-(void)setInfoWindowCallback_:(id)args
{
   // NSLog(@"set callback 1");
    
    //ENSURE_UI_THREAD(setInfoWindowView_, args);
    ENSURE_UI_THREAD(setInfoWindowCallback_, args);
    
    //if ([self square])
    {
        ENSURE_SINGLE_ARG(args,NSObject);
        
     //   NSLog(@"set callback 2");
        //ENSURE_TYPE(args, KrollCallback);
       // NSLog(@"set callback 3 : %@", args);
        
        //self.customView = args;
        
        self->customViewCallback = args;
        controller.customViewCallback = args;
        
    }

}


// left here as an example of a function that is called from
// the controller
/*
-(void)getInfoWindow:(id)args
{
    ENSURE_UI_THREAD(getInfoWindow, args);

    NSLog(@"!!!! 1");
    
    NSLog(@"data : %@", [args objectForKey:@"marker"]);
    id currentMarker = [args objectForKey:@"marker"];
    
    if(self->customViewCallback){
        NSLog(@"!!!! 3 : %@", self->customViewCallback);
        NSLog(@"!!!! 4 : %@", currentMarker);
        NSDictionary* dict = [[NSDictionary alloc] initWithObjectsAndKeys:currentMarker, @"marker" , nil];
        
        NSArray* array = [NSArray arrayWithObjects: dict, nil];
        NSLog(@"!!!! !!!! !!!! data2 : %@", array);
        TiViewProxy * viewProxy;
        viewProxy = (TiViewProxy *) [self->customViewCallback call:array thisObject:nil];
        NSLog(@"!!!! 5");
        
        self.customInfoWindow = viewProxy.view;

        return;
    }
    
    NSLog(@"!!!! no callback");
}
*/

-(void)removeOnClose_:(id)args
{
    //self.removeOnClose = true;
}

-(void)setCamera_:(id)args
{
    ENSURE_UI_THREAD(setCamera_, args);
    //if ([self square])
    {
        
        ENSURE_SINGLE_ARG(args,NSDictionary);

        GMSCameraPosition *camera;

        if ( ([args objectForKey:@"bearing"] != nil) || ([args objectForKey:@"viewingAngle"] != nil) )
        {
            camera = [GMSCameraPosition
                     cameraWithLatitude:[[args objectForKey:@"latitude"] floatValue]
                     longitude:[[args objectForKey:@"longitude"] floatValue]
                     zoom:[[args objectForKey:@"zoom"] integerValue]
                     bearing:[[args objectForKey:@"bearing"] floatValue]
                     viewingAngle:[[args objectForKey:@"viewingAngle"] floatValue]];
        }
        else
        {
            camera = [GMSCameraPosition
                     cameraWithLatitude:[[args objectForKey:@"latitude"] floatValue]
                     longitude:[[args objectForKey:@"longitude"] floatValue]
                     zoom:[[args objectForKey:@"zoom"] integerValue]];
        }

       
        [map setCamera:camera];
    }
    
    //NSLog(@"==> in setCamera, map: %@", map);
}




-(void)addPolyline:(id)args
{
    ENSURE_UI_THREAD(addPolyline, args);
    
    //if ([self square])
    {
        ENSURE_SINGLE_ARG(args,NSObject);
        
        ComMoshemarcianoGoogleMapsPolylineProxy * proxy = args;
        
        proxy.polyline.map = controller.mapView;
        [polylines addObject: proxy];
    }
}

-(void)removePolyline:(id)args
{
    ENSURE_UI_THREAD(removePolyline, args);
    
    ////if ([self square])
    {
        ENSURE_SINGLE_ARG(args,NSObject);
        ComMoshemarcianoGoogleMapsPolylineProxy * proxy = args;
        
        if (proxy.polyline)
        {
            [polylines removeObject:proxy];
            proxy.polyline.map = nil;
            //[proxy.polyline release];
            proxy.polyline = nil;
        }
    }
}

-(void)addPolygon:(id)args
{
    ENSURE_UI_THREAD(addPolygon, args);
    
    //if ([self square])
    {
        ENSURE_SINGLE_ARG(args,NSObject);
        
        ComMoshemarcianoGoogleMapsPolygonProxy * proxy = args;
        
        proxy.polygon.map = controller.mapView;
        [polygones addObject: proxy];
    }
}

-(void)removePolygon:(id)args
{
    ENSURE_UI_THREAD(removePolygon, args);

    ////if ([self square])
    {
        ENSURE_SINGLE_ARG(args,NSObject);
        ComMoshemarcianoGoogleMapsPolygonProxy * proxy = args;
        
        if (proxy.polygon)
        {
            [polygones removeObject:proxy];
            proxy.polygon.map = nil;
            //[proxy.polygon release];
            proxy.polygon = nil;
        }
    }
}


-(void)addCircle:(id)args
{
    ENSURE_UI_THREAD(addCircle, args);

    //if ([self square])
    {
        ENSURE_SINGLE_ARG(args,NSObject);
        
        ComMoshemarcianoGoogleMapsCircleProxy * proxy = args;
        
        proxy.circle.map = controller.mapView;
        [circles addObject: proxy];
    }
}

-(void)removeCircle:(id)args
{
    ENSURE_UI_THREAD(removeCircle, args);

    ////if ([self square])
    {
        ENSURE_SINGLE_ARG(args,NSObject);
        ComMoshemarcianoGoogleMapsCircleProxy * proxy = args;
        
        if (proxy.circle)
        {
            [circles removeObject:proxy];
            proxy.circle.map = nil;
            //[proxy.circle release];
            proxy.circle = nil;
        }
    }
}

-(void)addMarker:(id)args
{
   // ENSURE_UI_THREAD(addMarker, args);

    //if ([self square])
    {
        ENSURE_SINGLE_ARG(args, NSObject);
        
        ComMoshemarcianoGoogleMapsMarkerProxy * proxy = args;
        
        proxy.marker.map = controller.mapView;
        [markers addObject: proxy];
    }
}


-(void)removeMarker:(id)args
{
    ENSURE_UI_THREAD(removeMarker, args);

    ////if ([self square])
    {
        ENSURE_SINGLE_ARG(args,NSObject);
        ComMoshemarcianoGoogleMapsMarkerProxy * proxy = args;
       
       // if (proxy.marker)
        {
            [markers removeObject:proxy];
            proxy.marker.map = nil;
            //[proxy.marker release];
            proxy.marker = nil;
        }
    }
}


-(void)selectMarker:(id)args
{
    ENSURE_UI_THREAD(selectMarker, args);

    //if ([self square])
    {
        ENSURE_SINGLE_ARG(args,NSObject);
        ComMoshemarcianoGoogleMapsMarkerProxy * proxy = args;
        
        if (proxy.marker)
        {
            map.selectedMarker = proxy.marker;
        }
    }
}





-(void)clear:(id)args
{
    ENSURE_UI_THREAD(clear, args);
    
    //if ([self square])
    {
        [map clear];
    }
}

//////////////////// ANIMATIONS ////////////////////////

-(void)animateToViewingAngle:(id)args
{
    ENSURE_UI_THREAD(animateToViewingAngle, args);
    [map animateToViewingAngle:[TiUtils floatValue:args]];
}

-(void)animateToZoom:(id)args
{
    ENSURE_UI_THREAD(animateToZoom, args);
    [map animateToZoom:[TiUtils intValue:args]];
}

-(void)animateToBearing:(id)args
{
    ENSURE_UI_THREAD(animateToBearing, args);
    [map animateToBearing:[TiUtils floatValue:args]];
}


-(void)animateToCameraPosition:(id)args
{
    ENSURE_UI_THREAD(animateToCameraPosition, args);

    //if ([self square])
    {
        ENSURE_SINGLE_ARG(args,NSDictionary);
        
        GMSCameraPosition *camera;
        
        camera = [GMSCameraPosition
                  cameraWithLatitude:[[args objectForKey:@"latitude"] floatValue]
                  longitude:[[args objectForKey:@"longitude"] floatValue]
                  zoom:[[args objectForKey:@"zoom"] integerValue]
                  bearing:[[args objectForKey:@"bearing"] floatValue]
                  viewingAngle:[[args objectForKey:@"viewingAngle"] floatValue]];
        
        [map animateToCameraPosition:camera];
    }
}




-(void)animateToLocation:(id)args
{
    ENSURE_UI_THREAD(animateToLocation, args);
    ENSURE_SINGLE_ARG(args,NSDictionary);
    
    CGFloat lat = [[args objectForKey:@"latitude"] floatValue];
    CGFloat lon = [[args objectForKey:@"longitude"] floatValue];

    [map animateToLocation:CLLocationCoordinate2DMake(lat,lon)];
}


-(void)setLicenseKey_:(id)license
{
    //NSString * license = [NSString stringWithString:];
    
   // NSLog(@"[INFO] setting license: %@",[TiUtils stringValue:license]);
    
    [GMSServices provideAPIKey:[TiUtils stringValue:license]];
    
    NSLog(@"[INFO] [GoogleMapsModule] license set");
}



-(CGRect)frameForButtonStyle:(int)buttonStyle
{
	return buttonStyle == 1 ? CGRectMake(0, 0, 159, 29) : CGRectMake(0, 0, 79, 29);
}

// Camera update

-(void)zoomIn:(id)args
{
    ENSURE_UI_THREAD(zoomIn, args);
    GMSCameraUpdate *update = [GMSCameraUpdate zoomIn];
    [map moveCamera:update];
}

-(void)zoomOut:(id)args
{
    ENSURE_UI_THREAD(zoomOut, args);
    GMSCameraUpdate *update = [GMSCameraUpdate zoomOut];
    [map moveCamera:update];
}

-(void)zoomTo:(id)args
{
    ENSURE_UI_THREAD(zoomTo, args);
    GMSCameraUpdate *update = [GMSCameraUpdate zoomTo:[TiUtils floatValue:args]];
    [map moveCamera:update];
}

-(void)setTarget_:(id)args
{
     ENSURE_UI_THREAD(setTarget_, args);
    
    //if ([self square])
    {
        ENSURE_SINGLE_ARG(args,NSDictionary);
        
        GMSCameraUpdate *update;
        
        if ([args objectForKey:@"zoom"] == nil)
        {
            CGFloat lat = [[args objectForKey:@"latitude"] floatValue];
            CGFloat lon = [[args objectForKey:@"longitude"] floatValue];
            
            update = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(lat,lon)];
        }
        else
        {
            //NSLog(@"sssdjfsdkjflkdsflksdlkfsdlkjflskdjflkdsjfkldsjklfjdsklfjsdkljfsdf");
            CGFloat lat = [[args objectForKey:@"latitude"] floatValue];
            CGFloat lon = [[args objectForKey:@"longitude"] floatValue];
            CGFloat zoom = [[args objectForKey:@"zoom"] floatValue];
            
            update = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(lat,lon) zoom:zoom];
        }
        
        [map moveCamera:update];
    }
}

-(void)fitBounds:(id)args
{
    ENSURE_UI_THREAD(fitBounds, args);
    //if ([self square])
    {
        ENSURE_SINGLE_ARG(args,NSDictionary);
        GMSCoordinateBounds *bounds;

        CGFloat NElat   = [[args objectForKey:@"NElatitude"] floatValue];
        CGFloat NElon   = [[args objectForKey:@"NElongitude"] floatValue];
        
        CGFloat SWlat   = [[args objectForKey:@"SWlatitude"] floatValue];
        CGFloat SWlon   = [[args objectForKey:@"SWlongitude"] floatValue];

        CGFloat padding = [[args objectForKey:@"padding"] floatValue];

        bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:CLLocationCoordinate2DMake(NElat,NElon)
                                                      coordinate:CLLocationCoordinate2DMake(SWlat,SWlon)];
       
        GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds
                                             withPadding:padding];

        [map moveCamera:update];
    }
}

-(void)scrollBy:(id)args
{
    ENSURE_UI_THREAD(scrollBy, args);
    //if ([self square])
    {        
        CGFloat x = [TiUtils floatValue:args[0]];
        CGFloat y = [TiUtils floatValue:args[1]];
        
        GMSCameraUpdate *update = [GMSCameraUpdate scrollByX:x Y:y];
        
        [map moveCamera:update];
    }
}


-(CGFloat)example
{
    ENSURE_UI_THREAD_0_ARGS
    
    //map.camera;
    CGFloat c = map.camera.zoom;
    NSLog(@"aaaaaaaa: %f", c );
    return c;
    
    //return [NSNumber numberWithFloat:map.camera.zoom];
}

-(NSInteger)index_
{
    return 5;
}


-(id)zoom
{
    
    CGFloat c = map.camera.zoom;
    
    return NUMFLOAT(c);
}



-(id)currentRegion
{
    NSLog(@"currentRegion 2");
    NSMutableDictionary *event = [NSMutableDictionary dictionary];


    [event setObject:[NSNumber numberWithFloat:map.camera.target.latitude] forKey:@"latitude"];
    [event setObject:[NSNumber numberWithFloat:map.camera.target.longitude] forKey:@"longitude"];
    [event setObject:[NSNumber numberWithFloat:map.camera.bearing] forKey:@"bearing"];
    [event setObject:[NSNumber numberWithFloat:map.camera.zoom] forKey:@"zoomLevel"];
    [event setObject:[NSNumber numberWithFloat:map.camera.viewingAngle] forKey:@"viewingAngle"];
    
    return event;
}


-(id)mapType
{
    switch (map.mapType) {
        case kGMSTypeNormal:
            return @"normal";
        break;
            
        case kGMSTypeSatellite:
            return @"satellite";
            break;

        case kGMSTypeHybrid:
            return @"hybrid";
            break;

        case kGMSTypeTerrain:
            return @"terrian";
            break;

        case kGMSTypeNone:
            return @"none";
            break;

        default:
            break;
    }
   
    return NUMINT(@"error");
}



-(id)selectedMarker
{
    // ENSURE_UI_THREAD_0_ARGS
    
    // return @"Hi";
    
    //if ([self square])
    {
        // TODO not working
        
        ComMoshemarcianoGoogleMapsMarkerProxy * proxy = [[map selectedMarker] userData];
        NSLog(@"proxy: %@", [[[map selectedMarker] userData] myUserData]);
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:@"34343434" forKey:@"marker"];
        
        return proxy;//[[[map selectedMarker] userData] myUserData];
    }
}




@end
