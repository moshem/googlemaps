/**
 * Appcelerator Titanium Mobile
 * Copyright (c) 2009-2013 by Appcelerator, Inc. All Rights Reserved.
 * Licensed under the terms of the Apache Public License
 * Please see the LICENSE included with this distribution for details.
 */

#import "ComMoshemarcianoGoogleMapsGoogleMapProxy.h"
#import "TiUtils.h"
#import "TiViewProxy.h"
#import "ComMoshemarcianoGoogleMapsGoogleMap.h"
#import "ComMoshemarcianoGoogleMapsMarkerProxy.h"

@implementation ComMoshemarcianoGoogleMapsGoogleMapProxy

    @synthesize selectedMarker,bHotFixVR0407,mapType, propertyName;

//@synthesize _mapType;

/*
-(void)init:(id)args
{
    ENSURE_TYPE(args, NSArray);
    
    [(ComMoshemarcianoGoogleMapsGoogleMap *) [self view] performSelectorOnMainThread:@selector(init:) withObject:args waitUntilDone:YES];
}
*/

-(ComMoshemarcianoGoogleMapsGoogleMap *)map
{
    // NSLog(@"[GoogleMapsModule] ****************************************************");
	return (ComMoshemarcianoGoogleMapsGoogleMap *)[self view];
}


-(void)setHotFixVR0407:(id)value
{
    bHotFixVR0407 = [TiUtils boolValue:value];
    NSLog(@"[GoogleMapsModule] HotFixVR0407 set");
}

-(void)setCustomInfoWindow:(id)args
{
    ENSURE_UI_THREAD(setCustomInfoWindow, args);
    
    [[self view] performSelectorOnMainThread:@selector(setCustomInfoWindow_:)
                                  withObject:args waitUntilDone:NO];
}

-(void)setIndoorPicker:(id)args
{
    ENSURE_UI_THREAD(setIndoorPicker, args);
    
    [[self view] performSelectorOnMainThread:@selector(setIndoorPicker_:)
                                  withObject:args waitUntilDone:NO];
}

-(void)setScrollGestures:(id)args
{
    ENSURE_UI_THREAD(setScrollGestures, args);
    
    [[self view] performSelectorOnMainThread:@selector(setScrollGestures_:)
                                  withObject:args waitUntilDone:NO];
}

-(void)setZoomGestures:(id)args
{
    ENSURE_UI_THREAD(setZoomGestures, args);
    
    [[self view] performSelectorOnMainThread:@selector(setZoomGestures_:)
                                  withObject:args waitUntilDone:NO];
}

-(void)setTiltGestures:(id)args
{
    ENSURE_UI_THREAD(setTiltGestures, args);
    
    [[self view] performSelectorOnMainThread:@selector(setTiltGestures_:)
                                  withObject:args waitUntilDone:NO];
}

-(void)setRotateGestures:(id)args
{
    ENSURE_UI_THREAD(setRotateGestures, args);
    
    [[self view] performSelectorOnMainThread:@selector(setRotateGestures_:)
                                  withObject:args waitUntilDone:NO];
}


-(void)setMapType:(id)args
{
    ENSURE_UI_THREAD(setMapType, args);
    
//    NSLog(@"map type1: %@", @"hello");
//    
//    sMapType = [TiUtils stringValue:args];
//    propertyName = [TiUtils stringValue:args];
//    
//  //  [self replaceValue:[TiUtils stringValue:args] forKey:@"mapType" notification:NO];
//    NSLog(@"map type2: %@", sMapType);

    [[self view] performSelectorOnMainThread:@selector(setMapType_:)
                                  withObject:args waitUntilDone:NO];
}

    

-(void)setTraffic:(id)args
{
    ENSURE_UI_THREAD(setTraffic, args);
    
    [[self view] performSelectorOnMainThread:@selector(setTraffic_:)
                                  withObject:args waitUntilDone:NO];
}

-(void)setMyLocation:(id)args
{
    ENSURE_UI_THREAD(setMyLocation, args);
    
    [[self view] performSelectorOnMainThread:@selector(setMyLocation_:)
                                  withObject:args waitUntilDone:NO];
}

-(void)setMyLocationButton:(id)args
{
    ENSURE_UI_THREAD(setMyLocationButton, args);
    
    [[self view] performSelectorOnMainThread:@selector(setMyLocationButton_:)
                                  withObject:args waitUntilDone:NO];
}

-(void)setCompassButton:(id)args
{
    ENSURE_UI_THREAD(setCompassButton, args);
    
    [[self view] performSelectorOnMainThread:@selector(setCompassButton_:)
                                  withObject:args waitUntilDone:NO];
}

-(void)setInfoWindowCallback:(id)args
{
    ENSURE_UI_THREAD(setInfoWindowCallback, args);
    
    [[self view] performSelectorOnMainThread:@selector(setInfoWindowCallback_:)
                                  withObject:args waitUntilDone:NO];
}


-(void)setCamera:(id)args
{
    ENSURE_UI_THREAD(setCamera, args);
    
    [[self view] performSelectorOnMainThread:@selector(setCamera_:)
                                  withObject:args waitUntilDone:NO];
}

-(void)setCameraMoveOnMarkerTap:(id)args
{
    ENSURE_UI_THREAD(setCameraMoveOnMarkerTap, args);
    
   // NSLog(@"[GoogleMapsModule] setCameraMoveOnMarkerTap**********************************************************");
    
    [[self view] performSelectorOnMainThread:@selector(setCameraMoveOnMarkerTap_:)
                                  withObject:args waitUntilDone:NO];
}


-(void)infoWindowCallback:(id)args
{
    ENSURE_UI_THREAD(infoWindowCallback, args);
    
    [[self view] performSelectorOnMainThread:@selector(infoWindowCallback:)
                                  withObject:args waitUntilDone:NO];
}




-(id)hotFixVR0407
{
    return  [NSNumber numberWithInt:bHotFixVR0407];
}


-(void)animateToViewingAngle:(id)args
{
    ENSURE_UI_THREAD(animateToViewingAngle, args);
    
    [[self view] performSelectorOnMainThread:@selector(animateToViewingAngle:)
                                  withObject:args waitUntilDone:NO];
}

-(void)animateToZoom:(id)args
{
    ENSURE_UI_THREAD(animateToZoom, args);
    
    [[self view] performSelectorOnMainThread:@selector(animateToZoom:)
                                  withObject:args waitUntilDone:NO];
}


-(void)animateToBearing:(id)args
{
    ENSURE_UI_THREAD(animateToBearing, args);
    
    [[self view] performSelectorOnMainThread:@selector(animateToBearing:)
                                  withObject:args waitUntilDone:NO];
}

-(void)animateToLocation:(id)args
{
    ENSURE_UI_THREAD(animateToLocation, args);
    
    [[self view] performSelectorOnMainThread:@selector(animateToLocation:)
                                  withObject:args waitUntilDone:NO];
}

-(void)animateToCameraPosition:(id)args
{
    ENSURE_UI_THREAD(animateToCameraPosition, args);
    
    [[self view] performSelectorOnMainThread:@selector(animateToCameraPosition:)
                                  withObject:args waitUntilDone:NO];
}

-(void)addMarker:(id)args
{
    ENSURE_UI_THREAD(addMarker, args);
   
    [[self view] performSelectorOnMainThread:@selector(addMarker:)
                                  withObject:args waitUntilDone:YES];
}

-(void)selectMarker:(id)args
{
    ENSURE_UI_THREAD(selectMarker, args);
    
    [[self view] performSelectorOnMainThread:@selector(selectMarker:)
                                  withObject:args waitUntilDone:YES];
}


-(void)addPolyline:(id)args
{
    ENSURE_UI_THREAD(addPolyline, args);
    
    [[self view] performSelectorOnMainThread:@selector(addPolyline:)
                                  withObject:args waitUntilDone:YES];
}

-(void)removePolyline:(id)args
{
    ENSURE_UI_THREAD(removePolyline, args);
    
    [[self view] performSelectorOnMainThread:@selector(removePolyline:)
                                  withObject:args waitUntilDone:NO];
}

-(void)addPolygon:(id)args
{
    ENSURE_UI_THREAD(addPolygon, args);
    
    [[self view] performSelectorOnMainThread:@selector(addPolygon:)
                                  withObject:args waitUntilDone:YES];
}

-(void)removePolygon:(id)args
{
    ENSURE_UI_THREAD(removePolygon, args);
    
    [[self view] performSelectorOnMainThread:@selector(removePolygon:)
                                  withObject:args waitUntilDone:NO];
}

-(void)addCircle:(id)args
{
    ENSURE_UI_THREAD(addCircle, args);
    
    [[self view] performSelectorOnMainThread:@selector(addCircle:)
                                  withObject:args waitUntilDone:YES];
}

-(void)removeCircle:(id)args
{
    ENSURE_UI_THREAD(removeCircle, args);
    
    [[self view] performSelectorOnMainThread:@selector(removeCircle:)
                                  withObject:args waitUntilDone:NO];
}


-(void)removeMarker:(id)args
{
    ENSURE_UI_THREAD(removeMarker, args);
    
    [[self view] performSelectorOnMainThread:@selector(removeMarker:)
                                  withObject:args waitUntilDone:NO];
}

-(void)removeMap:(id)args
{
    ENSURE_UI_THREAD(removeMap, args);
    
    NSLog(@"[INFO] [GoogleMapsModule] Google Maps => removeMap");

    [[self view] performSelectorOnMainThread:@selector(removeMap:)
                                  withObject:args waitUntilDone:NO];
}


-(void)clear:(id)args
{
    ENSURE_UI_THREAD(clear, args);
    
    [[self view] performSelectorOnMainThread:@selector(clear:)
                                  withObject:args waitUntilDone:NO];    
}
/*
-(id)selectedMarker
{
    ENSURE_UI_THREAD_0_ARGS
    
    ComMoshemarcianoGoogleMapsMarkerProxy * p = [self map].selectedMarker;
    //NSLog(@"proxy2: %@", [self map].selectedMarker);
    return [self map].selectedMarker;
}
*/
-(void)zoomIn:(id)args
{
    ENSURE_UI_THREAD(zoomIn, args);
    
    [[self view] performSelectorOnMainThread:@selector(zoomIn:)
                                  withObject:args waitUntilDone:NO];    
}

-(void)zoomOut:(id)args
{
    ENSURE_UI_THREAD(zoomOut, args);
    
    [[self view] performSelectorOnMainThread:@selector(zoomOut:)
                                  withObject:args waitUntilDone:NO];
}

-(void)zoomTo:(id)args
{
    ENSURE_UI_THREAD(zoomTo, args);
    
    [[self view] performSelectorOnMainThread:@selector(zoomTo:)
                                  withObject:args waitUntilDone:NO];
}


-(void)fitBounds:(id)args
{
    ENSURE_UI_THREAD(fitBounds, args);
    
    [[self view] performSelectorOnMainThread:@selector(fitBounds:)
                                  withObject:args waitUntilDone:NO];
}

-(void)scrollBy:(id)args
{
    ENSURE_UI_THREAD(scrollBy, args);
    
    [[self view] performSelectorOnMainThread:@selector(scrollBy:)
                                  withObject:args waitUntilDone:NO];
}

-(id)example
{
    //ComMoshemarcianoGoogleMapsGoogleMap * p = self.view;
    
    CGFloat zoomy = [(ComMoshemarcianoGoogleMapsGoogleMap *)[self view] example];
    
    NSLog(@"bbbbbbb: %f", zoomy );
    
   // id ret = [NSNumber numberWithDouble:[(ComMoshemarcianoGoogleMapsGoogleMap *)[self view] zoom]];
	
    return [NSNumber numberWithDouble:zoomy];
}

-(id)index
{
    //return @"abc";
    return [NSNumber numberWithDouble:12.0];
}

-(id)zoom
{
    __block NSNumber *z;
    
    TiThreadPerformOnMainThread(^{

        z = [(ComMoshemarcianoGoogleMapsGoogleMap *)[self view] zoom];
    
    },YES);
    
    return z;
}

-(id)currentRegion
{
    __block id r;
    
    TiThreadPerformOnMainThread(^{
        
        r = [(ComMoshemarcianoGoogleMapsGoogleMap *)[self view] currentRegion];
        
    },YES);
    
    return r;
}

-(id)mapType
{
    __block NSNumber *r;
    
    TiThreadPerformOnMainThread(^{
        
        r = [(ComMoshemarcianoGoogleMapsGoogleMap *)[self view] mapType];
        
    },YES);
    
    return r;
}

-(id)selectedMarker
{
    __block id r;
    
    TiThreadPerformOnMainThread(^{
        
        r = [(ComMoshemarcianoGoogleMapsGoogleMap *)[self view] selectedMarker];
        
    },YES);
    
    return r;
}


/*
-(id)init
{
	// This is the designated initializer method and will always be called
	// when the view proxy is created.
	
	NSLog(@"[VIEWPROXY LIFECYCLE EVENT] init");
    ENSURE_UI_THREAD_0_ARGS;
   
    
    [[self view] performSelectorOnMainThread:@selector(init:)
                                  withObject:nil waitUntilDone:NO];
	
	return [super init];
 
 ENSURE_UI_THREAD_0_ARGS
 
 ComMoshemarcianoGoogleMapsMarkerProxy * p = [self map].selectedMarker;
 NSLog(@"proxy2: %@", [self map].selectedMarker);
 return [self map].selectedMarker;
}
*/
/*
-(void)_destroy
{
	// This method is called from the dealloc method and is good place to
	// release any objects and memory that have been allocated for the view proxy.
	
	NSLog(@"[VIEWPROXY LIFECYCLE EVENT] _destroy");

	[super _destroy];
}

-(void)dealloc
{
	// This method is called when the view proxy is being deallocated. The superclass
	// method calls the _destroy method.
	
	NSLog(@"[VIEWPROXY LIFECYCLE EVENT] dealloc : %@", ([self viewAttached] == YES));
    
    return;
	

    
//	[super dealloc];
}

-(id)_initWithPageContext:(id<TiEvaluator>)context
{
	// This method is one of the initializers for the view proxy class. If the
	// proxy is created without arguments then this initializer will be called.
	// This method is also called from the other _initWithPageContext method.
	// The superclass method calls the init and _configure methods.
	
	NSLog(@"[VIEWPROXY LIFECYCLE EVENT] _initWithPageContext (no arguments)");
	
	return [super _initWithPageContext:context];
}

-(id)_initWithPageContext:(id<TiEvaluator>)context_ args:(NSArray*)args
{
	// This method is one of the initializers for the view proxy class. If the
	// proxy is created with arguments then this initializer will be called.
	// The superclass method calls the _initWithPageContext method without
	// arguments.
	
	NSLog(@"[VIEWPROXY LIFECYCLE EVENT] _initWithPageContext %@", args);
	
	return [super _initWithPageContext:context_ args:args];
}
*/
-(void)_configure
{
	// This method is called from _initWithPageContext to allow for
	// custom configuration of the module before startup. The superclass
	// method calls the startup method.
	
//	NSLog(@"[VIEWPROXY LIFECYCLE EVENT] _configure");

    ComMoshemarcianoGoogleMapsGoogleMap * map = (ComMoshemarcianoGoogleMapsGoogleMap *)[self view];
    
    [map performSelectorOnMainThread:@selector(square)
                          withObject:nil waitUntilDone:NO];

    
	[super _configure];
}
    
//- (NSString *)title
//{
//    ENSURE_UI_THREAD_0_ARGS
//    
//    NSString *string = sMapType;
//    
//    NSLog(@"[test] type: %@", sMapType);
//
//    return (NSString *) sMapType;
//}
//    
    
/*
-(void)_initWithProperties:(NSDictionary *)properties
{
	// This method is called from _initWithPageContext if arguments have been
	// used to create the view proxy. It is called after the initializers have completed
	// and is a good point to process arguments that have been passed to the
	// view proxy create method since most of the initialization has been completed
	// at this point.
	
	NSLog(@"[VIEWPROXY LIFECYCLE EVENT] _initWithProperties %@", properties);
	
    
	[super _initWithProperties:properties];
    
}

-(void)viewWillAttach
{
	// This method is called right before the view is attached to the proxy
	
	NSLog(@"[VIEWPROXY LIFECYCLE EVENT] viewWillAttach");
}

-(void)viewDidAttach
{
	// This method is called right after the view has attached to the proxy
    
	NSLog(@"[VIEWPROXY LIFECYCLE EVENT] viewDidAttach");
    
    
}

-(void)viewDidDetach
{
	// This method is called right before the view is detached from the proxy
	
	NSLog(@"[VIEWPROXY LIFECYCLE EVENT] viewDidDetach");
  

}

-(void)viewWillDetach
{
	// This method is called right after the view has detached from the proxy
	
	NSLog(@"[VIEWPROXY LIFECYCLE EVENT] viewWillDetach");
}
*/
@end

