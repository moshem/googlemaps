/**
 * Appcelerator Titanium Mobile
 * Copyright (c) 2009-2013 by Appcelerator, Inc. All Rights Reserved.
 * Licensed under the terms of the Apache Public License
 * Please see the LICENSE included with this distribution for details.
 */
#import "TiUIView.h"
#import "MapSampleViewController.h"
#import "ComMoshemarcianoGoogleMapsMarkerProxy.h"


@interface ComMoshemarcianoGoogleMapsGoogleMap : TiUIView {
    UIView * square;
    MapSampleViewController * controller;
    GMSMapView * map;
    //BOOL isShutdown;
    
    NSMutableArray * markers;
    NSMutableArray * circles;
    NSMutableArray * polylines;
    NSMutableArray * polygones;
    KrollCallback *  customViewCallback;
    

  //  id mapType;
}

//@property(nonatomic,readwrite,assign) id selectedMarker;
@property(nonatomic,readwrite,assign) CGFloat example;
@property(nonatomic,readwrite,assign) id zoom;
@property(nonatomic,readwrite,assign) id getMap;
@property(nonatomic,readwrite,assign) UIView * customInfoWindow;





//@property(nonatomic,readwrite,assign) BOOL isShutdown;
//@property(nonatomic,readwrite,assign) NSString *mapType;

//-(void)removeMap;
-(id)currentRegion;
-(id)mapType;
-(id)selectedMarker;
-(void)getInfoWindow;
@end
