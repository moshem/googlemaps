/**
 * Appcelerator Titanium Mobile
 * Copyright (c) 2009-2013 by Appcelerator, Inc. All Rights Reserved.
 * Licensed under the terms of the Apache Public License
 * Please see the LICENSE included with this distribution for details.
 */

#import "ComMoshemarcianoGoogleMapsCircleProxy.h"
#import "TiUtils.h"

@implementation ComMoshemarcianoGoogleMapsCircleProxy
@synthesize circle;

-(GMSCircle *)circle
{
    if (circle==nil)
    {
        circle = [[GMSCircle alloc] init];
    }
    
    return circle;
}

-(void)setPosition:(id)value
{
    ENSURE_UI_THREAD(setPosition, value);
    
    if ([self circle])
    {
        NSInteger itemCount = [value count];
        if (itemCount % 2)
        {
            NSLog (@"[GoogleMapsModule] uneven number of position data passed, aborting");
            return;
        }
        
        CLLocationCoordinate2D circleCenter = CLLocationCoordinate2DMake([value[0] doubleValue], [value[1] doubleValue]);
        circle.position = circleCenter;
    }
}

-(void)setTappable:(id)value
{
    ENSURE_UI_THREAD(setTappable, value);
    
    if ([self circle])
    {
        circle.tappable = [TiUtils boolValue:value];
    }
}

-(void)setTitle:(id)value
{
    ENSURE_UI_THREAD(setTitle, value);
    
    if ([self circle])
    {
        circle.title = [TiUtils stringValue:value];
    }
}

-(void)setLocation:(id)value
{
    ENSURE_UI_THREAD(setLocation, value);
    
    if ([self circle])
    {
        self.circle.position = CLLocationCoordinate2DMake([[value objectForKey:@"latitude"] doubleValue],[[value objectForKey:@"longitude"] doubleValue]);
    }
}

-(void)setRadius:(id)value
{
    ENSURE_UI_THREAD(setRadius, value);
    
    if ([self circle])
    {
        circle.radius = [TiUtils floatValue:value];
    }
}

-(void)setWidth:(id)value
{
    ENSURE_UI_THREAD(setWidth, value);
    
    if ([self circle])
    {
        circle.strokeWidth = [TiUtils floatValue:value];
    }
}

-(void)setColor:(id)value
{
    ENSURE_UI_THREAD(setColor, value);
    
    if ([self circle])
    {
        circle.strokeColor = [[TiUtils colorValue:value] _color];
    }
}

-(void)setFillColor:(id)value
{
    ENSURE_UI_THREAD(setFillColor, value);
    
    if ([self circle])
    {
        circle.fillColor = [[TiUtils colorValue:value] _color];
    }
}


-(void)setAccessibilityLabel:(id)value
{
    ENSURE_UI_THREAD(setAccessibilityLabel, value);
    
    if ([self circle])
    {
        circle.accessibilityLabel = [TiUtils stringValue:value];
    }
}



@end
