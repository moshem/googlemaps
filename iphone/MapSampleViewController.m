#import "MapSampleViewController.h"

#import <GoogleMaps/GoogleMaps.h>

#import "ComMoshemarcianoGoogleMapsMarkerProxy.h"
#import "ComMoshemarcianoGoogleMapsGoogleMapProxy.h"
#import "ComMoshemarcianoGoogleMapsGoogleMap.h"

@interface MapSampleViewController ()

//- (void)updateLeftButtons;

@end

#pragma mark - MyLocationObserver

// MyLocationObserver is a helper class to observe myLocation. It's required
// to be disjoint from the MapSampleViewController as subclasses may want to
// listen to myLocation on its own.
@interface MyLocationObserver : NSObject

- (id)initWithController:(MapSampleViewController *)controller;

@end
/*
@implementation MyLocationObserver {
  __weak MapSampleViewController *controller_;
}


- (id)initWithController:(MapSampleViewController *)controller {
  if ((self = [super init])) {
    controller_ = controller;
  }
  return self;
}


- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
//  [controller_ updateLeftButtons];
}

@end
*/
#pragma mark - MapSampleViewController

@implementation MapSampleViewController {
  GMSMapView *mapView_;
  //TiUIView *viewAttached_;
  //MyLocationObserver *observer_;
  //UIBarButtonItem *samplesButton_;
}

//@synthesize latitude, longitude;
@synthesize viewAttached, customInfoWindow, overrideCameraMoveOnTapMarker, isShutdown, customViewCallback;

- (id)init {
  if ((self = [super init])) {
 //   self.title = [[self class] description];
   // observer_ = [[MyLocationObserver alloc] initWithController:self];
     ;
  }
  return self;
}

- (void)loadView {
  self.view = self.mapView;
    //NSLog(@"===> 2");
}

- (void)dealloc {
//  [self.mapView removeObserver:observer_ forKeyPath:@"myLocation"];
    //NSLog(@"[INFO] [GoogleMapsModule] Google Maps => Controller dealloc");

    //[mapView_ release];
    //[super dealloc];
}

- (GMSMapView *)mapView {

    if (mapView_ == nil) {
    
        NSLog(@"[INFO] [GoogleMapsModule] Google Maps => New map instance");

        mapView_ = [[GMSMapView alloc] initWithFrame:CGRectZero];
        
        /*GMSCameraPosition *camera;

        camera = [GMSCameraPosition
                  cameraWithLatitude:51.43580627441406
                  longitude:-0.14256912469863892
                  zoom:5];

        [mapView_ setCamera:camera];*/

        mapView_.delegate = self;

        NSMutableDictionary *event = [NSMutableDictionary dictionary];
        
        [event setObject:@"loaded" forKey:@"mapView"];
        
        
        [[viewAttached proxy] fireEvent:@"mapLoaded" withObject:event];
        
        // init
        
//        [mapView_ setMyLocationEnabled:NO];
//        [mapView_ setTrafficEnabled:NO];
    }
    

  return mapView_;
}


// The default implementation of updateSamplesButton takes over the left-hand
// buttons on this controller's navigation bar.
/*- (void)updateSamplesButton:(UIBarButtonItem *)samplesButton {
  samplesButton_ = samplesButton;
  [self updateLeftButtons];
}

+ (NSString *)notes {
  return nil;
}
*/
#pragma mark - Private methods
/*
// Update the buttons on the left of this VC's UINavigationItem.
- (void)updateLeftButtons {
  NSMutableArray *buttons = [NSMutableArray array];

  if (samplesButton_) {
    [buttons addObject:samplesButton_];
  }

  if (mapView_.myLocation != nil) {
    UIBarButtonSystemItem myLocationItem = 100;  // "My Location"
    SEL action = @selector(didTapMyLocation);
    UIBarButtonItem *button =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:myLocationItem
                                                      target:self
                                                      action:action];
    [buttons addObject:button];
  }

  self.navigationItem.leftItemsSupplementBackButton = YES;
  self.navigationItem.leftBarButtonItems = buttons;
}

// Animate to the user's location.
- (void)didTapMyLocation {
  [mapView_ animateToLocation:mapView_.myLocation.coordinate];
  [mapView_ animateToBearing:0];
  [mapView_ animateToViewingAngle:0];
}
*/



- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position
{

//    NSLog(@"idleAtCameraPosition: %@", position);
    
    NSMutableDictionary *event = [NSMutableDictionary dictionary];
    
    [event setObject:[NSNumber numberWithFloat:position.target.latitude] forKey:@"latitude"];
    [event setObject:[NSNumber numberWithFloat:position.target.longitude] forKey:@"longitude"];
    [event setObject:[NSNumber numberWithFloat:position.bearing] forKey:@"bearing"];
    [event setObject:[NSNumber numberWithFloat:position.zoom] forKey:@"zoomLevel"];
    [event setObject:[NSNumber numberWithFloat:position.viewingAngle] forKey:@"viewingAngle"];
    
    [[viewAttached proxy] fireEvent:@"idleAtCameraPosition" withObject:event];
    
}


/**
 * Called after a tap gesture at a particular coordinate, but only if a marker
 * was not tapped.  This is called before deselecting any currently selected
 * marker (the implicit action for tapping on the map).
 */

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    NSLog(@"You tapped at COORD");
    
    NSMutableDictionary *event = [NSMutableDictionary dictionary];
    CGPoint viewCord =
    [[mapView projection] pointForCoordinate:coordinate];
    
    [event setObject:[NSNumber numberWithFloat:coordinate.latitude] forKey:@"latitude"];
    [event setObject:[NSNumber numberWithFloat:coordinate.longitude] forKey:@"longitude"];
    [event setObject:[NSNumber numberWithInt:viewCord.x] forKey:@"mapX"];
    [event setObject:[NSNumber numberWithInt:viewCord.y] forKey:@"mapY"];
    
    [[viewAttached proxy] fireEvent:@"tapAtCoordinate" withObject:event];

        //NSLog(@"You tapped at %f,%f, fired to view %@", coordinate.latitude, coordinate.longitude, viewAttached);
}

/**
 * Called after the camera position has changed. During an animation, this
 * delegate might not be notified of intermediate camera positions. However, it
 * will always be called eventually with the final position of an the animation.
 */

 - (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position;
{
    NSMutableDictionary *event = [NSMutableDictionary dictionary];

    
    
    [event setObject:[NSNumber numberWithFloat:position.target.latitude] forKey:@"latitude"];
    [event setObject:[NSNumber numberWithFloat:position.target.longitude] forKey:@"longitude"];
    [event setObject:[NSNumber numberWithFloat:position.bearing] forKey:@"bearing"];
    [event setObject:[NSNumber numberWithFloat:position.zoom] forKey:@"zoomLevel"];
    [event setObject:[NSNumber numberWithFloat:position.viewingAngle] forKey:@"viewingAngle"];
    
    ComMoshemarcianoGoogleMapsGoogleMapProxy * proxy = (ComMoshemarcianoGoogleMapsGoogleMapProxy *)[viewAttached proxy];
    
    if ([proxy bHotFixVR0407])
    {
        NSLog(@"[INFO] [GoogleMapsModule] Applying SDK HotFIX (HotFixVR0407)");
        CGPoint topLeftPoint = CGPointMake(0, 0);
        CLLocationCoordinate2D topLeftLocation =
        [mapView.projection coordinateForPoint: topLeftPoint];
        
        CGPoint bottomRightPoint =
        CGPointMake(mapView.frame.size.width, mapView.frame.size.height);
        CLLocationCoordinate2D bottomRightLocation =
        [mapView.projection coordinateForPoint: bottomRightPoint];
        
        CGPoint topRightPoint = CGPointMake(mapView.frame.size.width, 0);
        CLLocationCoordinate2D topRightLocation =
        [mapView.projection coordinateForPoint: topRightPoint];
        
        CGPoint bottomLeftPoint =
        CGPointMake(0, mapView.frame.size.height);
        CLLocationCoordinate2D bottomLeftLocation =
        [mapView.projection coordinateForPoint: bottomLeftPoint];
        
        GMSVisibleRegion realVisibleRegion;
        realVisibleRegion.farLeft = topLeftLocation;
        realVisibleRegion.farRight = topRightLocation;
        realVisibleRegion.nearLeft = bottomLeftLocation;
        realVisibleRegion.nearRight = bottomRightLocation;
        
        [event setObject:[NSNumber numberWithFloat:topLeftLocation.latitude] forKey:@"visibleRegion.farLeft.latitude"];
        
        [event setObject:[NSNumber numberWithFloat:topLeftLocation.longitude] forKey:@"visibleRegion.farLeft.longitude"];
        
        [event setObject:[NSNumber numberWithFloat:topRightLocation.latitude] forKey:@"visibleRegion.farRight.latitude"];
        
        [event setObject:[NSNumber numberWithFloat:topRightLocation.longitude] forKey:@"visibleRegion.farRight.longitude"];
        
        [event setObject:[NSNumber numberWithFloat:bottomLeftLocation.latitude] forKey:@"visibleRegion.nearLeft.latitude"];
        
        [event setObject:[NSNumber numberWithFloat:bottomLeftLocation.longitude] forKey:@"visibleRegion.nearLeft.longitude"];
        
        [event setObject:[NSNumber numberWithFloat:bottomRightLocation.latitude] forKey:@"visibleRegion.nearRight.latitude"];
        
        [event setObject:[NSNumber numberWithFloat:bottomRightLocation.longitude] forKey:@"visibleRegion.nearRight.longitude"];
        
        [[viewAttached proxy] fireEvent:@"changeCameraPosition" withObject:event];
        
        return;
    }
    
    
    [event setObject:[NSNumber numberWithFloat:mapView.projection.visibleRegion.farLeft.latitude] forKey:@"visibleRegion.farLeft.latitude"];

    [event setObject:[NSNumber numberWithFloat:mapView.projection.visibleRegion.farLeft.longitude] forKey:@"visibleRegion.farLeft.longitude"];

    [event setObject:[NSNumber numberWithFloat:mapView.projection.visibleRegion.farRight.latitude] forKey:@"visibleRegion.farRight.latitude"];
    
    [event setObject:[NSNumber numberWithFloat:mapView.projection.visibleRegion.farRight.longitude] forKey:@"visibleRegion.farRight.longitude"];
    
    [event setObject:[NSNumber numberWithFloat:mapView.projection.visibleRegion.nearLeft.latitude] forKey:@"visibleRegion.nearLeft.latitude"];
    
    [event setObject:[NSNumber numberWithFloat:mapView.projection.visibleRegion.nearLeft.longitude] forKey:@"visibleRegion.nearLeft.longitude"];
    
    [event setObject:[NSNumber numberWithFloat:mapView.projection.visibleRegion.nearRight.latitude] forKey:@"visibleRegion.nearRight.latitude"];
    
    [event setObject:[NSNumber numberWithFloat:mapView.projection.visibleRegion.nearRight.longitude] forKey:@"visibleRegion.nearRight.longitude"];
    
    
    
    [[viewAttached proxy] fireEvent:@"changeCameraPosition" withObject:event];
}


/**
 * Called after a long-press gesture at a particular coordinate.
 *
 * @param mapView The map view that was pressed.
 * @param coordinate The location that was pressed.
 */
- (void)mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate;
{
    NSMutableDictionary *event = [NSMutableDictionary dictionary];

    CGPoint viewCord =
    [[mapView projection] pointForCoordinate:coordinate];
    
    [event setObject:[NSNumber numberWithFloat:coordinate.latitude] forKey:@"latitude"];
    [event setObject:[NSNumber numberWithFloat:coordinate.longitude] forKey:@"longitude"];
    [event setObject:[NSNumber numberWithInt:viewCord.x] forKey:@"mapX"];
    [event setObject:[NSNumber numberWithInt:viewCord.y] forKey:@"mapY"];
    
    [[viewAttached proxy] fireEvent:@"longPressAtCoordinate" withObject:event];
}


- (void)mapView:(GMSMapView *)mapView didTapOverlay:(GMSOverlay *)overlay
{
    // When a polygon is tapped, randomly change its fill color to a new hue.
    if ([overlay isKindOfClass:[GMSPolygon class]])
    {
       // NSLog(@"overlay tapped");
 
        NSMutableDictionary *event = [NSMutableDictionary dictionary];
        [event setObject:@"polygon" forKey:@"overlayType"];
        if ([overlay title] != nil)
            [event setObject:[overlay title] forKey:@"title"];
        
        [[viewAttached proxy] fireEvent:@"tapOverlay" withObject:event];
    }
}
/**
 * Called after a marker has been tapped.
 *
 * @param mapView The map view that was pressed.
 * @param marker The marker that was pressed.
 * @return YES if this delegate handled the tap event, which prevents the map
 *         from performing its default selection behavior, and NO if the map
 *         should continue with its default selection behavior.
 */
- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    NSLog(@"You tapped marker at %@", [marker userData]);
    
    if ( (customInfoWindow == NO) && (overrideCameraMoveOnTapMarker == YES) )
        mapView.selectedMarker = marker;
    

    NSMutableDictionary *event = [NSMutableDictionary dictionary];

    CGPoint viewCord =
    [[mapView projection] pointForCoordinate:CLLocationCoordinate2DMake(marker.position.latitude, marker.position.longitude)];
    
    [event setObject:[NSNumber numberWithFloat:marker.position.latitude] forKey:@"latitude"];
    [event setObject:[NSNumber numberWithFloat:marker.position.longitude] forKey:@"longitude"];
    [event setObject:[marker title] forKey:@"title"];

    if ([ [marker userData] userData] != nil)
        [event setObject:(ComMoshemarcianoGoogleMapsMarkerProxy *)[ [marker userData] userData] forKey:@"userData"];
   
    [event setObject:[marker userData] forKey:@"marker"];
    [event setObject:[NSNumber numberWithInt:viewCord.x] forKey:@"mapX"];
    [event setObject:[NSNumber numberWithInt:viewCord.y] forKey:@"mapY"];
     
     
    
    [[viewAttached proxy] fireEvent:@"tapMarker" withObject:event];
    //NSLog(@"MARKER TAP");
    
    if ( (customInfoWindow == NO) && (overrideCameraMoveOnTapMarker == YES) )
        return YES;
    
    if (customInfoWindow == NO)
        return NO;
    else
        return YES;
}

/**
 * Called after a marker's info window has been tapped.
 */

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker
{
    NSMutableDictionary *event = [NSMutableDictionary dictionary];
    CGPoint viewCord =
    [[mapView projection] pointForCoordinate:CLLocationCoordinate2DMake(marker.position.latitude, marker.position.longitude)];
   
    [event setObject:[NSNumber numberWithFloat:marker.position.latitude] forKey:@"latitude"];
    [event setObject:[NSNumber numberWithFloat:marker.position.longitude] forKey:@"longitude"];
    [event setObject:[marker title] forKey:@"title"];
    [event setObject:[NSNumber numberWithInt:viewCord.x] forKey:@"mapX"];
    [event setObject:[NSNumber numberWithInt:viewCord.y] forKey:@"mapY"];
    
    if ([ [marker userData] userData] != nil)
        [event setObject:(ComMoshemarcianoGoogleMapsMarkerProxy *)[ [marker userData] userData] forKey:@"userData"];
    
    [[viewAttached proxy] fireEvent:@"tapInfoWindowOfMarker" withObject:event];
}

/**
 * Called when a marker is about to become selected, and provides an optional
 * custom info window to use for that marker if this method returns a UIView.
 * If you change this view after this method is called, those changes will not
 * necessarily be reflected in the rendered version.
 *
 * The returned UIView must not have bounds greater than 500 points on either
 * dimension.  As there is only one info window shown at any time, the returned
 * view may be reused between other info windows.
 *
 * @return The custom info window for the specified marker, or nil for default
 */
- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker
{
    
    if (!self->customViewCallback)
        return nil;
    
//    NSLog(@"You tapped at %@", self.customInfoWindow);
    

    NSDictionary* dict = [[NSDictionary alloc] initWithObjectsAndKeys:[marker userData], @"marker" , nil];
    
    NSArray* array = [NSArray arrayWithObjects: dict, nil];
    TiViewProxy * viewProxy;
    viewProxy = (TiViewProxy *) [self->customViewCallback call:array thisObject:nil];
    
    return viewProxy.view;
    
//  left here for an example of how to call a method on the view

/*
    ComMoshemarcianoGoogleMapsGoogleMap * map = (ComMoshemarcianoGoogleMapsGoogleMap *)[self viewAttached];


    NSMutableDictionary *event = [NSMutableDictionary dictionary];
    [event setObject:[marker userData] forKey:@"marker"];

    [map performSelectorOnMainThread:@selector(getInfoWindow:)
                          withObject:event waitUntilDone:YES];
    
   return map.customInfoWindow;
 
 */
}

- (void)mapView:(GMSMapView *)mapView didBeginDraggingMarker:(GMSMarker *)marker
{
    
    NSMutableDictionary *event = [NSMutableDictionary dictionary];
    
    CGPoint viewCord =
    [[mapView projection] pointForCoordinate:CLLocationCoordinate2DMake(marker.position.latitude, marker.position.longitude)];
    
    [event setObject:[NSNumber numberWithFloat:marker.position.latitude] forKey:@"latitude"];
    [event setObject:[NSNumber numberWithFloat:marker.position.longitude] forKey:@"longitude"];
    [event setObject:[marker title] forKey:@"title"];
    
    if ([ [marker userData] userData] != nil)
        [event setObject:(ComMoshemarcianoGoogleMapsMarkerProxy *)[ [marker userData] userData] forKey:@"userData"];
    
    [event setObject:[marker userData] forKey:@"marker"];
    [event setObject:[NSNumber numberWithInt:viewCord.x] forKey:@"mapX"];
    [event setObject:[NSNumber numberWithInt:viewCord.y] forKey:@"mapY"];
    
    
    
    [[viewAttached proxy] fireEvent:@"dragMarkerBegin" withObject:event];
    
    return nil;
}


- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker
{
    
    NSMutableDictionary *event = [NSMutableDictionary dictionary];
    
    CGPoint viewCord =
    [[mapView projection] pointForCoordinate:CLLocationCoordinate2DMake(marker.position.latitude, marker.position.longitude)];
    
    [event setObject:[NSNumber numberWithFloat:marker.position.latitude] forKey:@"latitude"];
    [event setObject:[NSNumber numberWithFloat:marker.position.longitude] forKey:@"longitude"];
    [event setObject:[marker title] forKey:@"title"];
    
    if ([ [marker userData] userData] != nil)
        [event setObject:(ComMoshemarcianoGoogleMapsMarkerProxy *)[ [marker userData] userData] forKey:@"userData"];
    
    [event setObject:[marker userData] forKey:@"marker"];
    [event setObject:[NSNumber numberWithInt:viewCord.x] forKey:@"mapX"];
    [event setObject:[NSNumber numberWithInt:viewCord.y] forKey:@"mapY"];
    
    
    
    [[viewAttached proxy] fireEvent:@"dragMarkerEnd" withObject:event];
    
    return nil;
}

- (void)mapView:(GMSMapView *)mapView didDragMarker:(GMSMarker *)marker
{
    
    NSMutableDictionary *event = [NSMutableDictionary dictionary];
    
    CGPoint viewCord =
    [[mapView projection] pointForCoordinate:CLLocationCoordinate2DMake(marker.position.latitude, marker.position.longitude)];
    
    [event setObject:[NSNumber numberWithFloat:marker.position.latitude] forKey:@"latitude"];
    [event setObject:[NSNumber numberWithFloat:marker.position.longitude] forKey:@"longitude"];
    [event setObject:[marker title] forKey:@"title"];
    
    if ([ [marker userData] userData] != nil)
        [event setObject:(ComMoshemarcianoGoogleMapsMarkerProxy *)[ [marker userData] userData] forKey:@"userData"];
    
    [event setObject:[marker userData] forKey:@"marker"];
    [event setObject:[NSNumber numberWithInt:viewCord.x] forKey:@"mapX"];
    [event setObject:[NSNumber numberWithInt:viewCord.y] forKey:@"mapY"];
    
    
    
    [[viewAttached proxy] fireEvent:@"draggingMarker" withObject:event];
    
    return nil;
}



/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSLog(@"===> 1");

}
*/
- (void)viewDidDisappear:(BOOL)animated {
    
    // find the parent's viewController
    
    UIViewController * mainViewController;
    
    for (UIView* next = [self.view superview]; next; next = next.superview)
    {
        UIResponder* nextResponder = [next nextResponder];
        
        if ([nextResponder isKindOfClass:[UIViewController class]])
        {
            mainViewController =  (UIViewController*)nextResponder;
        }
    }
    
    NSArray * viewControllers = mainViewController.navigationController.viewControllers;
    
    // check if parent viewcontroller is in UINavigationController
    // and if so, if it was desposed of or just pushed in the heirechy
    
    if (viewControllers.count > 1 && [viewControllers objectAtIndex:viewControllers.count-2] == mainViewController) {
        
        // View is disappearing because a new view controller was pushed onto the stack
        // do nothing
        
//        NSLog(@"New view controller was pushed");
    } else if (([viewControllers indexOfObject:mainViewController] == NSNotFound) || (mainViewController == nil)) {
        
        // view is disposed, clear all map data
    
      //  ComMoshemarcianoGoogleMapsGoogleMap * mapObject = (ComMoshemarcianoGoogleMapsGoogleMap*) viewAttached;
        
    //    [mapView_ clear] ;
        //[mapView_ stopRendering] ;
       // [mapView_ removeFromSuperview] ;
       // [viewAttached removeMap];
        //[viewAttached removeFromSuperview];
        //mapView_ = nil;
        //self.view = nil;
        //self.isShutdown = YES;
        
        ComMoshemarcianoGoogleMapsGoogleMap * map = (ComMoshemarcianoGoogleMapsGoogleMap *)[self viewAttached];
        
        [map performSelectorOnMainThread:@selector(removeMap)
                              withObject:nil waitUntilDone:NO];
        
//        NSLog(@"View controller was popped");
    } else {
    //    NSLog(@"View controller DONT KNOW WHAT JUST HAPPENED");
//        NSLog(@"View controller count: %u", viewControllers.count);
    }
    

    [super viewDidDisappear:animated];
}





@end
